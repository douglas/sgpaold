============================================
SGPA - Sistema Gerenciador de Projetos Ageis
============================================

TBW
TODO: Utilizar Markdown

Instalação de dependências
==========================

Dependendo de onde você estiver instalando as dependências:

Em desenvolvimento::

    $ pip install -r requirements/local.pykgs

Em produção::

    $ pip install -r requirements.txt

*nota 1: Nós instalamos as dependências de produção dessa forma porque várias plataformas como serviço (PaaS) esperam um 
um arquivo requirements.txt na raiz dos projetos.*

*nota 2: As extensões .pykgs são utilizadas apenas pra associar à extensão INI do SublimeText, ficando com highlight.*

Créditos
========

    - PyDanny
    - Audrey Roy

.. _colaboradores do template usado: https://bitbucket.org/douglas/sgpa/raw/18d99ad43b9f6b72ac7cd7dcef8ab5610db6b50e/CONTRIBUTORS.txt
