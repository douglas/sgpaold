from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='base.html')),
    url(r'^login/', TemplateView.as_view(template_name='signin.html')),
    url(r'^sgpa/', TemplateView.as_view(template_name='index.html')),

    # Examples:
    # url(r'^$', 'sgpa_project.views.home', name='home'),
    # url(r'^sgpa_project/', include('sgpa_project.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # user panel toolbar
    url(r'', include('debug_toolbar_user_panel.urls')),
)
